﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration; 

namespace Utilities
{
    public class dbms_configuration
    {
        public static string GetConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["mainConn"].ConnectionString; }
        }

        public static string GetCompanyName
        {
            get { return ConfigurationManager.AppSettings["companyName"]; }
        }

    }
}
