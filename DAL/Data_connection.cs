﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Utilities; 

namespace DAL
{
    public class Data_connection
    {
        public static SqlCommand CreateDBConnection()
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = dbms_configuration.GetConnectionString;
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            return cmd;
        }

        public static int ExecuteNonQueryCommand(SqlCommand cmd)
        {
            int i = -1;
            try
            {
                cmd.Connection.Open();
                i = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return i;
        }


        public static DataTable ExecuteReaderCommand(SqlCommand cmd)
        {
            DataTable tbl = new DataTable();
            try
            {
                cmd.Connection.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                tbl.Load(rdr);
                rdr.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
            }

            return tbl;
        }

    }
}
