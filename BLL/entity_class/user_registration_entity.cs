﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.entity_class
{
    public class user_registration_entity
    {
        public string student_id_mgmt { get; set; }
        public string student_id_sci { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string email_id { get; set; }
        public string address { get; set; }
        public string phone_no { get; set; }
        public string user_type { get; set; } // true stands for admin and false stands for common users
        public string password { get; set; }
    }
}
