﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.entity_class
{
    public class pm_registration_form_entity
    {
        public int id { get; set; }
        public  string title { get; set; }
        public  DateTime start_date { get; set; }
        public  DateTime end_date { get; set; }
        public  string client_name { get; set; }
        public string description { get; set; }
        public string programmer_id { get; set; }
      


    }
}
