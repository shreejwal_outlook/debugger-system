﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BLL.entity_class
{
    public class pm_tester_entity
    {
        public string project_title { get; set; }
        public DateTime completion_date { get; set; }
        public DateTime end_date { get; set; }
        public string programmer_id { get; set; }
        public string tester_id { get; set; }
    }
}
