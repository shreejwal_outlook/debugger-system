﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using BLL.entity_class;
using DAL;

namespace BLL.manager_class
{
    public class pm_tester_manager
    {
        public static bool tester_entry(string type, pm_tester_entity pe)
        {
            SqlCommand cmd = Data_connection.CreateDBConnection();
            cmd.CommandText = "tester_project_p";
            if (type == "insert")
            {
                cmd.Parameters.AddWithValue("@char", type);
            }
            cmd.Parameters.AddWithValue("@project_title", pe.project_title);
            cmd.Parameters.AddWithValue("@completion_date", pe.completion_date);
            cmd.Parameters.AddWithValue("@end_date_date", pe.end_date);
            cmd.Parameters.AddWithValue("@programmer_id", pe.programmer_id);
            cmd.Parameters.AddWithValue("@tester_id", pe.tester_id);
          


            return (Data_connection.ExecuteNonQueryCommand(cmd) >= 0);

        }


    }
}
