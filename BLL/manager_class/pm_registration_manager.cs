﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using BLL.entity_class;
using DAL; 

namespace BLL.manager_class
{
    public class pm_registration_manager
    {
        public static bool user_reg_entry(string type, pm_registration_form_entity pm)
        {
            SqlCommand cmd = Data_connection.CreateDBConnection();
            cmd.CommandText = "project_manager_p";
            if (type == "insert")
            {
                cmd.Parameters.AddWithValue("@char", type);
            }
            cmd.Parameters.AddWithValue("@title",pm.title);
            cmd.Parameters.AddWithValue("@start_date",pm.start_date);
            cmd.Parameters.AddWithValue("@end_date", pm.end_date);
            cmd.Parameters.AddWithValue("@client_name", pm.client_name);
            cmd.Parameters.AddWithValue("@description", pm.description);
            cmd.Parameters.AddWithValue("@programmer_id", pm.programmer_id);
       

            return (Data_connection.ExecuteNonQueryCommand(cmd) >= 0);

        }


        public static bool user_reg_update(string type, pm_registration_form_entity pm)
        {
            SqlCommand cmd = Data_connection.CreateDBConnection();
            cmd.CommandText = "project_manager_p";
            if (type == "update")
            {
                cmd.Parameters.AddWithValue("@char", type);
                cmd.Parameters.AddWithValue("@id", pm.id);
            }
            cmd.Parameters.AddWithValue("@title", pm.title);
            cmd.Parameters.AddWithValue("@start_date", pm.start_date);
            cmd.Parameters.AddWithValue("@end_date", pm.end_date);
            cmd.Parameters.AddWithValue("@client_name", pm.client_name);
            cmd.Parameters.AddWithValue("@description", pm.description);
            cmd.Parameters.AddWithValue("@programmer_id", pm.programmer_id);
            cmd.Parameters.AddWithValue("@modified_date", DateTime.Now);


            return (Data_connection.ExecuteNonQueryCommand(cmd) >= 0);

        }

        public static DataTable project_view(string type)
        {
            SqlCommand cmd = Data_connection.CreateDBConnection();
            cmd.CommandText = "project_manager_p";
            cmd.Parameters.AddWithValue("@char", type);
        

            return (Data_connection.ExecuteReaderCommand(cmd));
        }

        public static DataTable completion_project_view(string type)
        {
            SqlCommand cmd = Data_connection.CreateDBConnection();
            cmd.CommandText = "programmer_project_P";
            cmd.Parameters.AddWithValue("@char", type);


            return (Data_connection.ExecuteReaderCommand(cmd));
        }

        public static DataTable my_project_view(string type,pm_registration_form_entity my_data)
        {
            SqlCommand cmd = Data_connection.CreateDBConnection();
            cmd.CommandText = "project_manager_p";
            cmd.Parameters.AddWithValue("@char", type);
            cmd.Parameters.AddWithValue("@programmer_id",my_data.programmer_id);


            return (Data_connection.ExecuteReaderCommand(cmd));
        }

        public static DataTable project_del(string type,pm_registration_form_entity project)
        {
            SqlCommand cmd = Data_connection.CreateDBConnection();
            cmd.CommandText = "project_manager_p";
            cmd.Parameters.AddWithValue("@char", type);
            cmd.Parameters.AddWithValue("@id", project.id );
           

            return (Data_connection.ExecuteReaderCommand(cmd));
        }
    }
}
