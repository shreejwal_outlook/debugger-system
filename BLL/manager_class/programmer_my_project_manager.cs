﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using BLL.entity_class;
using DAL; 

namespace BLL.manager_class
{
    public class programmer_my_project_manager
    {
        public static bool programmer_project_entry(string type, programmer_my_project_entity pm)
        {
            SqlCommand cmd = Data_connection.CreateDBConnection();
            cmd.CommandText = "programmer_project_p";
            if (type == "insert")
            {
                cmd.Parameters.AddWithValue("@char", type);
            }
            cmd.Parameters.AddWithValue("@project_registration_id", pm.project_registration_id);
            cmd.Parameters.AddWithValue("@completion_date", pm.completion_date);
      


            return (Data_connection.ExecuteNonQueryCommand(cmd) >= 0);

        }

    }
}
