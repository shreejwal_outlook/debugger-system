﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using BLL.entity_class;
using DAL; 

namespace BLL.manager_class
{
    public class login_manager
    {
        public static DataTable user_select(string type, login_entity login)
        {
            SqlCommand cmd = Data_connection.CreateDBConnection();
            cmd.CommandText = "login_p";

            cmd.Parameters.AddWithValue("@email_id", login.email);
            cmd.Parameters.AddWithValue("@password", login.password);
            return (Data_connection.ExecuteReaderCommand(cmd));
        }

        //public static DataTable student_select(string type, faculty_entity email_id)
        //{
        //    SqlCommand cmd = Data_connection.CreateDBConnection();
        //    cmd.CommandText = "user_p";
        //    cmd.Parameters.AddWithValue("@char", type);
        //    cmd.Parameters.AddWithValue("@email", email_id.email);

        //    return (Data_connection.ExecuteReaderCommand(cmd));
        //}


    }
}
