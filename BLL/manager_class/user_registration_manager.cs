﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using BLL.entity_class;
using DAL; 

namespace BLL.manager_class
{
   public class user_registration_manager
    {

        public static bool user_reg_entry(string type, user_registration_entity user_reg)
        {
            SqlCommand cmd = Data_connection.CreateDBConnection();
            cmd.CommandText = "user_p";
            if (type == "insert")
            {
                cmd.Parameters.AddWithValue("@char", type);
            }
            cmd.Parameters.AddWithValue("@first_name", user_reg.first_name);
            cmd.Parameters.AddWithValue("@last_name", user_reg.last_name);
            cmd.Parameters.AddWithValue("@email", user_reg.email_id);
            cmd.Parameters.AddWithValue("@address", user_reg.address);
            cmd.Parameters.AddWithValue("@phone_no", user_reg.phone_no);
            string user_type_v = "";

            if (user_reg.user_type == "PM")
            {
                user_type_v = "PM";
            }
            else if (user_reg.user_type == "P")
            {
                user_type_v = "P";
            }
            else if (user_reg.user_type == "ST")
            {
                user_type_v = "ST";
            }

            cmd.Parameters.AddWithValue("@user_type", user_type_v);
            cmd.Parameters.AddWithValue("@password", user_reg.password);
            return (Data_connection.ExecuteNonQueryCommand(cmd) >= 0);

        }

        public static DataTable programmer_data(string type) {
            SqlCommand cmd = Data_connection.CreateDBConnection();
            cmd.Parameters.AddWithValue("@char", type);
            cmd.CommandText = "user_p";

            return (Data_connection.ExecuteReaderCommand(cmd));
        }

        public static DataTable user_view(string type)
        {
            SqlCommand cmd = Data_connection.CreateDBConnection();
            cmd.Parameters.AddWithValue("@char", type);
            cmd.CommandText = "user_p";

            return (Data_connection.ExecuteReaderCommand(cmd));
        }

        public static DataTable tester_view(string type)
        {
            SqlCommand cmd = Data_connection.CreateDBConnection();
            cmd.Parameters.AddWithValue("@char", type);
            cmd.CommandText = "user_p";

            return (Data_connection.ExecuteReaderCommand(cmd));
        }

        public static DataTable student_name_select_mgmt(string type, user_registration_entity user)
        {
            SqlCommand cmd = Data_connection.CreateDBConnection();
            cmd.CommandText = "user_p";
            cmd.Parameters.AddWithValue("@char", type);
            cmd.Parameters.AddWithValue("@user_id", user.student_id_mgmt);


            return (Data_connection.ExecuteReaderCommand(cmd));
        }

        public static DataTable student_name_select_sci(string type, user_registration_entity user)
        {
            SqlCommand cmd = Data_connection.CreateDBConnection();
            cmd.CommandText = "user_p";
            cmd.Parameters.AddWithValue("@char", type);
            cmd.Parameters.AddWithValue("@user_id", user.student_id_sci);


            return (Data_connection.ExecuteReaderCommand(cmd));
        }


    }
}
