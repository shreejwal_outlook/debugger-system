﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;
using BLL.entity_class;
using BLL.manager_class;
 


namespace bug_dubugger_system
{
    public partial class login : Form
    {
        public string email; 
        public login()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            login_entity login = new login_entity();
            login.email = txt_email.Text;
            login.password = txt_password.Text;

            login_manager.user_select("select", login);

            DataTable tbl = login_manager.user_select("view", login);
            if (tbl.Rows.Count > 0)
            {

                //management_faculty_entity me = new management_faculty_entity();
                string data = tbl.Rows[0]["user_type"].ToString(); // True stands for admin and false stands for other users
                email = tbl.Rows[0]["email"].ToString();
                pm_registration_form_entity pm_reg = new  pm_registration_form_entity();
                pm_reg.programmer_id = email; 
                if (data == "PM")
                {
                    //MessageBox.Show("this is projectmanager");
                    project_manager_panel p = new project_manager_panel();
                    this.Hide();
                    p.Show(); 
                }
                else if (data == "ST")
                {
                    MessageBox.Show("this is system tester");
                }

                else if (data == "P ")
                {
                    //MessageBox.Show("this is programmer");
                    programmer_my_projects mp = new programmer_my_projects();
                    mp.Show(); 

                }
            }
            else if (txt_email.Text == "admin" && txt_password.Text == "admin")
            {
                MessageBox.Show("This is the  super admin");
            }
            else
            {
                MessageBox.Show("Error in Login");
            }
        }

        private void signupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            user_registration up = new user_registration();
            this.Hide();
            up.Show(); 
        }

        private void homeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            login l = new login();
            this.Hide();
            l.Show(); 
        }
    }
}
