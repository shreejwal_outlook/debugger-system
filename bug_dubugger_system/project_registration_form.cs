﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;
using BLL.entity_class;
using BLL.manager_class; 

namespace bug_dubugger_system
{
    public partial class project_registration_form : Form
    {
        public project_registration_form()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            pm_registration_form_entity registration = new pm_registration_form_entity();
            registration.title = txt_title.Text;
            registration.start_date = dt_start_date.Value.Date;
            registration.end_date = dt_end_date.Value.Date;
            registration.client_name = txt_client_name.Text;
            registration.description = txt_description.Text;
            registration.programmer_id = drp_programmer_assigned.SelectedItem.ToString(); 
            pm_registration_manager.user_reg_entry("insert",registration);

            
        }

        private void txt_description_TextChanged(object sender, EventArgs e)
        {

        }

        private void project_registration_form_Load(object sender, EventArgs e)
        {

            DataTable tbl = user_registration_manager.programmer_data("programmer");
            if (tbl.Rows.Count > 0)
            {
                foreach (DataRow item in tbl.Rows)
                {
                    //string data = tbl.Rows[0]["email"].ToString(); // True stands for admin and false stands for other users
                    string data = item["email"].ToString();
                    drp_programmer_assigned.Items.Add(data);
                }
            }
        }

        private void pMPanelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            project_manager_panel pp = new project_manager_panel();
            this.Hide();
            pp.Show(); 
        }
    }
}
