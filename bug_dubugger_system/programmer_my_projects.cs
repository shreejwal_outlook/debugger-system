﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;
using BLL.manager_class;
using BLL.entity_class;


namespace bug_dubugger_system
{
    public partial class programmer_my_projects : Form
    {
        int selectRow,project_id; 
        public programmer_my_projects()
        {
            InitializeComponent();
        }

        private void programmer_my_projects_Load(object sender, EventArgs e)
        {

            DataTable tbl = pm_registration_manager.project_view("view");
            grd_project_data.Rows.Clear();
            foreach (DataRow item in tbl.Rows)
            {
                int n = grd_project_data.Rows.Add();


                grd_project_data.Rows[n].Cells[0].Value = item["title"].ToString();
                grd_project_data.Rows[n].Cells[1].Value = item["description"].ToString();
                grd_project_data.Rows[n].Cells[2].Value = item["start_date"].ToString();
                grd_project_data.Rows[n].Cells[3].Value = item["end_date"].ToString();
                //grd_project_data.Rows[n].Cells[5].Value = item["description"].ToString();
                grd_project_data.Rows[n].Cells[4].Value = item["client_name"].ToString();
                project_id = int.Parse(item["id"].ToString()); 
                
              
         

            }
        }

        private void grd_project_data_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            programmer_my_project_entity my_project = new programmer_my_project_entity();
            my_project.project_registration_id = project_id;
            my_project.completion_date = dt_completion_date.Value.Date; 
            programmer_my_project_manager.programmer_project_entry("insert",my_project);
        }

        private void grd_project_data_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            selectRow = e.RowIndex;
            DataGridViewRow row = grd_project_data.Rows[selectRow];
            lbl_title.Text = row.Cells[0].Value.ToString();
            txt_description.Text = row.Cells[1].Value.ToString();
           lbl_start_date.Text = row.Cells[2].Value.ToString();
            lbl_end_date.Text = row.Cells[3].Value.ToString();
            lbl_client_name.Text = row.Cells[4].Value.ToString();
           


        }
    }
}
