﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bug_dubugger_system
{
    public partial class project_manager_panel : Form
    {
        public project_manager_panel()
        {
            InitializeComponent();
        }

        private void btn_project_registration_Click(object sender, EventArgs e)
        {
            project_registration_form pr = new project_registration_form();
            this.Hide();
            pr.Show(); 
        }

        private void project_manager_panel_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            project_manager_data pd = new project_manager_data();
            this.Hide();
            pd.Show(); 
        }
    }
}
