﻿namespace bug_dubugger_system
{
    partial class user_registration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(user_registration));
            this.homeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btn_submit = new System.Windows.Forms.Button();
            this.drp_usertype = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_phone = new System.Windows.Forms.TextBox();
            this.btn_password = new System.Windows.Forms.Button();
            this.txt_password = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.box_password = new System.Windows.Forms.GroupBox();
            this.txt_address = new System.Windows.Forms.TextBox();
            this.txt_emailid = new System.Windows.Forms.TextBox();
            this.txt_fname = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.box_user_reg = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.box_password.SuspendLayout();
            this.box_user_reg.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // homeToolStripMenuItem
            // 
            this.homeToolStripMenuItem.Name = "homeToolStripMenuItem";
            this.homeToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.homeToolStripMenuItem.Text = "Home";
            this.homeToolStripMenuItem.Click += new System.EventHandler(this.homeToolStripMenuItem_Click);
            // 
            // btn_submit
            // 
            this.btn_submit.BackColor = System.Drawing.SystemColors.HighlightText;
            this.btn_submit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_submit.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.btn_submit.Location = new System.Drawing.Point(445, 172);
            this.btn_submit.Name = "btn_submit";
            this.btn_submit.Size = new System.Drawing.Size(76, 33);
            this.btn_submit.TabIndex = 12;
            this.btn_submit.Text = "Submit";
            this.btn_submit.UseVisualStyleBackColor = false;
            this.btn_submit.Click += new System.EventHandler(this.btn_submit_Click);
            // 
            // drp_usertype
            // 
            this.drp_usertype.FormattingEnabled = true;
            this.drp_usertype.Items.AddRange(new object[] {
            "Project Manager",
            "Programmer",
            "System Tester"});
            this.drp_usertype.Location = new System.Drawing.Point(302, 142);
            this.drp_usertype.Name = "drp_usertype";
            this.drp_usertype.Size = new System.Drawing.Size(121, 21);
            this.drp_usertype.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(198, 120);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(38, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Phone";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(198, 142);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "UserType";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(198, 94);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Address";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(198, 68);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Email Id";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(198, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Full Name";
            // 
            // txt_phone
            // 
            this.txt_phone.Location = new System.Drawing.Point(302, 113);
            this.txt_phone.Name = "txt_phone";
            this.txt_phone.Size = new System.Drawing.Size(100, 20);
            this.txt_phone.TabIndex = 3;
            // 
            // btn_password
            // 
            this.btn_password.BackColor = System.Drawing.SystemColors.HighlightText;
            this.btn_password.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_password.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.btn_password.Location = new System.Drawing.Point(445, 13);
            this.btn_password.Name = "btn_password";
            this.btn_password.Size = new System.Drawing.Size(76, 33);
            this.btn_password.TabIndex = 13;
            this.btn_password.Text = "Submit";
            this.btn_password.UseVisualStyleBackColor = false;
            this.btn_password.Click += new System.EventHandler(this.btn_password_Click);
            // 
            // txt_password
            // 
            this.txt_password.Location = new System.Drawing.Point(174, 23);
            this.txt_password.Name = "txt_password";
            this.txt_password.PasswordChar = '*';
            this.txt_password.Size = new System.Drawing.Size(249, 20);
            this.txt_password.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(19, 26);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(109, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Enter Your Password ";
            // 
            // box_password
            // 
            this.box_password.Controls.Add(this.btn_password);
            this.box_password.Controls.Add(this.txt_password);
            this.box_password.Controls.Add(this.label7);
            this.box_password.Location = new System.Drawing.Point(119, 155);
            this.box_password.Name = "box_password";
            this.box_password.Size = new System.Drawing.Size(539, 54);
            this.box_password.TabIndex = 5;
            this.box_password.TabStop = false;
            // 
            // txt_address
            // 
            this.txt_address.Location = new System.Drawing.Point(302, 87);
            this.txt_address.Name = "txt_address";
            this.txt_address.Size = new System.Drawing.Size(100, 20);
            this.txt_address.TabIndex = 2;
            // 
            // txt_emailid
            // 
            this.txt_emailid.Location = new System.Drawing.Point(302, 61);
            this.txt_emailid.Name = "txt_emailid";
            this.txt_emailid.Size = new System.Drawing.Size(100, 20);
            this.txt_emailid.TabIndex = 1;
            // 
            // txt_fname
            // 
            this.txt_fname.Location = new System.Drawing.Point(302, 35);
            this.txt_fname.Name = "txt_fname";
            this.txt_fname.Size = new System.Drawing.Size(100, 20);
            this.txt_fname.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(236, 89);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(325, 31);
            this.label1.TabIndex = 3;
            this.label1.Text = "User  Registration Form";
            // 
            // box_user_reg
            // 
            this.box_user_reg.Controls.Add(this.pictureBox1);
            this.box_user_reg.Controls.Add(this.btn_submit);
            this.box_user_reg.Controls.Add(this.drp_usertype);
            this.box_user_reg.Controls.Add(this.label6);
            this.box_user_reg.Controls.Add(this.label5);
            this.box_user_reg.Controls.Add(this.label4);
            this.box_user_reg.Controls.Add(this.label3);
            this.box_user_reg.Controls.Add(this.label2);
            this.box_user_reg.Controls.Add(this.txt_phone);
            this.box_user_reg.Controls.Add(this.txt_address);
            this.box_user_reg.Controls.Add(this.txt_emailid);
            this.box_user_reg.Controls.Add(this.txt_fname);
            this.box_user_reg.Location = new System.Drawing.Point(119, 215);
            this.box_user_reg.Name = "box_user_reg";
            this.box_user_reg.Size = new System.Drawing.Size(539, 211);
            this.box_user_reg.TabIndex = 4;
            this.box_user_reg.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(39, 24);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(138, 139);
            this.pictureBox1.TabIndex = 13;
            this.pictureBox1.TabStop = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.homeToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(784, 24);
            this.menuStrip1.TabIndex = 6;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // user_registration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(784, 461);
            this.Controls.Add(this.box_password);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.box_user_reg);
            this.Controls.Add(this.menuStrip1);
            this.Name = "user_registration";
            this.Text = "User Registration Form";
            this.Load += new System.EventHandler(this.user_registration_Load);
            this.box_password.ResumeLayout(false);
            this.box_password.PerformLayout();
            this.box_user_reg.ResumeLayout(false);
            this.box_user_reg.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStripMenuItem homeToolStripMenuItem;
        private System.Windows.Forms.Button btn_submit;
        private System.Windows.Forms.ComboBox drp_usertype;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_phone;
        private System.Windows.Forms.Button btn_password;
        private System.Windows.Forms.TextBox txt_password;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox box_password;
        private System.Windows.Forms.TextBox txt_address;
        private System.Windows.Forms.TextBox txt_emailid;
        private System.Windows.Forms.TextBox txt_fname;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox box_user_reg;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

