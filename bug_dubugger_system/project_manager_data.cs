﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;
using BLL.entity_class;
using BLL.manager_class;

namespace bug_dubugger_system
{
    public partial class project_manager_data : Form
    {
        int selectRow,id; 

        public project_manager_data()
        {
            InitializeComponent();
        }

        private void project_manager_data_Load(object sender, EventArgs e)
        {
            DataTable tbl_v = user_registration_manager.programmer_data("programmer");
            if (tbl_v.Rows.Count > 0)
            {
                foreach (DataRow item in tbl_v.Rows)
                {
                    //string data = tbl.Rows[0]["email"].ToString(); // True stands for admin and false stands for other users
                    string data = item["email"].ToString();
                    drp_programmer_id.Items.Add(data);
                }
            }

            DataTable tbl = pm_registration_manager.project_view("view");
            grd_project_data.Rows.Clear();
            foreach (DataRow item in tbl.Rows)
            {
                int n = grd_project_data.Rows.Add();


                grd_project_data.Rows[n].Cells[0].Value = item["title"].ToString();
                grd_project_data.Rows[n].Cells[1].Value = item["start_date"].ToString();
                grd_project_data.Rows[n].Cells[2].Value = item["end_date"].ToString();
                grd_project_data.Rows[n].Cells[3].Value = item["client_name"].ToString();
                //grd_project_data.Rows[n].Cells[5].Value = item["description"].ToString();
                grd_project_data.Rows[n].Cells[4].Value = item["programmer_id"].ToString();
                grd_project_data.Rows[n].Cells[5].Value = item["created_date"].ToString();
                id = int.Parse(item["id"].ToString()); 

            }

        }

        private void grd_project_data_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            selectRow = e.RowIndex;
            DataGridViewRow row = grd_project_data.Rows[selectRow];
            txt_title.Text = row.Cells[0].Value.ToString();
            dt_start_date.Text = row.Cells[1].Value.ToString();
            dt_end_date.Text = row.Cells[2].Value.ToString();
            txt_client_name.Text = row.Cells[3].Value.ToString();
            drp_programmer_id.Text = row.Cells[4].Value.ToString();
           
        }

        private void txt_title_TextChanged(object sender, EventArgs e)
        {

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {

        }

        private void grd_project_data_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            pm_registration_form_entity faculty = new pm_registration_form_entity();
            faculty.id = id;
            pm_registration_manager.project_del("delete", faculty);
            MessageBox.Show("Value Deleted successfully !!");
        }

        private void pMPanelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            project_manager_panel pp = new project_manager_panel();
            this.Hide();
            pp.Show(); 
        }

        private void button1_Click(object sender, EventArgs e)
        {
            pm_registration_form_entity registration = new pm_registration_form_entity();
            registration.title = txt_title.Text;
            registration.start_date = dt_start_date.Value.Date;
            registration.end_date = dt_end_date.Value.Date;
            registration.client_name = txt_client_name.Text;
            registration.programmer_id = drp_programmer_id.SelectedItem.ToString();
            registration.id = id; 
            pm_registration_manager.user_reg_update("update", registration);
        }
    }
}
