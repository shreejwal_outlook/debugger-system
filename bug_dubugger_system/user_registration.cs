﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL.entity_class;
using BLL.manager_class;

namespace bug_dubugger_system
{
    public partial class user_registration : Form
    {
        public user_registration()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void btn_submit_Click(object sender, EventArgs e)
        {
            try
            {

                if (txt_fname.Text == null || drp_usertype.SelectedItem.ToString() == null)
                {
                    MessageBox.Show("Please Fill the Data inside the field");
                }
                else
                {
                    box_user_reg.Hide();
                    box_password.Show();
                }
            }
            catch (Exception a)
            {
                MessageBox.Show("Error Found !");
            }
        }

        private void btn_password_Click(object sender, EventArgs e)
        {
            string first_name = "null";
            string second_name = "null";

            string full_name = txt_fname.Text;
            if (full_name != null)
            {
                string[] word = full_name.Split(' ');
                first_name = word[0];
                second_name = word[1];
            }

            string user_type = drp_usertype.SelectedItem.ToString();

            user_registration_entity user_reg = new user_registration_entity();
            user_reg.first_name = first_name;
            user_reg.last_name = second_name;
            user_reg.email_id = txt_emailid.Text;
            user_reg.phone_no = txt_phone.Text;
            user_reg.address = txt_address.Text;
            if (user_type == "Project Manager")
            {
               
                user_reg.user_type = "PM";
            }
            else if (user_type == "Programmer")
            {
                user_reg.user_type = "P";
            }
            else if (user_type == "System Tester")
            {
                user_reg.user_type = "ST";
            }
            user_reg.password = txt_password.Text;

            user_registration_manager.user_reg_entry("insert", user_reg);

            login l = new login();
            this.Hide();
            l.Show();
        }

        private void user_registration_Load(object sender, EventArgs e)
        {
            box_password.Hide();
        }

        private void homeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            project_manager_panel mp = new project_manager_panel();
            this.Hide();
            mp.Show(); 
        }
    }
}
