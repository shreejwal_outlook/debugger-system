﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;
using BLL.manager_class;
using BLL.entity_class;


namespace bug_dubugger_system
{
    public partial class project_manager_testing : Form
    {
        int selectRow; 
        public project_manager_testing()
        {
            InitializeComponent();
        }

        private void project_manager_testing_Load(object sender, EventArgs e)
        {
            DataTable tbl_v = user_registration_manager.tester_view("tester");
            foreach (DataRow item in tbl_v.Rows) {
                string email_id  = item["email"].ToString();
                drp_tester.Items.Add(email_id);
            }

            DataTable tbl = pm_registration_manager.completion_project_view("pm_p_view");
            grd_project_data.Rows.Clear();
            foreach (DataRow item in tbl.Rows)
            {
                int n = grd_project_data.Rows.Add();


                grd_project_data.Rows[n].Cells[0].Value = item["title"].ToString();
                grd_project_data.Rows[n].Cells[1].Value = item["programmer_id"].ToString();
                grd_project_data.Rows[n].Cells[2].Value = item["start_date"].ToString();
                grd_project_data.Rows[n].Cells[3].Value = item["end_date"].ToString();
                grd_project_data.Rows[n].Cells[4].Value = item["completion_date"].ToString();
                grd_project_data.Rows[n].Cells[5].Value = item["client_name"].ToString();
       
         
            }
        }

        private void grd_project_data_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            selectRow = e.RowIndex;
            DataGridViewRow row = grd_project_data.Rows[selectRow];
            lbl_title.Text = row.Cells[0].Value.ToString();
            lbl_programmer_id.Text = row.Cells[1].Value.ToString();
            lbl_start_date.Text = row.Cells[2].Value.ToString();
            lbl_end_date.Text = row.Cells[3].Value.ToString();
            lbl_completion_date.Text = row.Cells[4].Value.ToString();
            lbl_client_name.Text = row.Cells[5].Value.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            pm_tester_entity test = new pm_tester_entity();
            test.project_title = lbl_title.Text;
            test.completion_date = DateTime.Parse(lbl_completion_date.Text);
            test.end_date = DateTime.Parse(lbl_end_date.Text);
            test.programmer_id = lbl_programmer_id.Text;
            test.tester_id = drp_tester.SelectedItem.ToString();
            pm_tester_manager.tester_entry("insert",test);
                 
             
        }
    }
}
