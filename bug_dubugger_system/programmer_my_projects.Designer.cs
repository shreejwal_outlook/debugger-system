﻿namespace bug_dubugger_system
{
    partial class programmer_my_projects
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grd_project_data = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lbl_title = new System.Windows.Forms.Label();
            this.lbl_end_date = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lbl_client_name = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lbl_start_date = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txt_description = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.dt_completion_date = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.clm_title = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clm_description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clm_start_date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clm_end_date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clm_client = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.grd_project_data)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grd_project_data
            // 
            this.grd_project_data.AllowUserToAddRows = false;
            this.grd_project_data.AllowUserToDeleteRows = false;
            this.grd_project_data.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grd_project_data.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clm_title,
            this.clm_description,
            this.clm_start_date,
            this.clm_end_date,
            this.clm_client});
            this.grd_project_data.Location = new System.Drawing.Point(161, 135);
            this.grd_project_data.Name = "grd_project_data";
            this.grd_project_data.ReadOnly = true;
            this.grd_project_data.Size = new System.Drawing.Size(546, 124);
            this.grd_project_data.TabIndex = 1;
            this.grd_project_data.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grd_project_data_CellClick);
            this.grd_project_data.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grd_project_data_CellContentClick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.dt_completion_date);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.txt_description);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.lbl_start_date);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.lbl_client_name);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.lbl_end_date);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.lbl_title);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(117, 274);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(641, 250);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(30, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Project Title : ";
            // 
            // lbl_title
            // 
            this.lbl_title.AutoSize = true;
            this.lbl_title.Location = new System.Drawing.Point(122, 16);
            this.lbl_title.Name = "lbl_title";
            this.lbl_title.Size = new System.Drawing.Size(80, 13);
            this.lbl_title.TabIndex = 4;
            this.lbl_title.Text = "No Data Found";
            // 
            // lbl_end_date
            // 
            this.lbl_end_date.AutoSize = true;
            this.lbl_end_date.Location = new System.Drawing.Point(299, 57);
            this.lbl_end_date.Name = "lbl_end_date";
            this.lbl_end_date.Size = new System.Drawing.Size(80, 13);
            this.lbl_end_date.TabIndex = 6;
            this.lbl_end_date.Text = "No Data Found";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(221, 57);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "End Date :";
            // 
            // lbl_client_name
            // 
            this.lbl_client_name.AutoSize = true;
            this.lbl_client_name.Location = new System.Drawing.Point(122, 57);
            this.lbl_client_name.Name = "lbl_client_name";
            this.lbl_client_name.Size = new System.Drawing.Size(80, 13);
            this.lbl_client_name.TabIndex = 8;
            this.lbl_client_name.Text = "No Data Found";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(31, 57);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Client_name :";
            // 
            // lbl_start_date
            // 
            this.lbl_start_date.AutoSize = true;
            this.lbl_start_date.Location = new System.Drawing.Point(305, 16);
            this.lbl_start_date.Name = "lbl_start_date";
            this.lbl_start_date.Size = new System.Drawing.Size(80, 13);
            this.lbl_start_date.TabIndex = 12;
            this.lbl_start_date.Text = "No Data Found";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(221, 16);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(64, 13);
            this.label10.TabIndex = 11;
            this.label10.Text = "Start Date : ";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(31, 92);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(102, 13);
            this.label12.TabIndex = 13;
            this.label12.Text = "Project Description :";
            // 
            // txt_description
            // 
            this.txt_description.Enabled = false;
            this.txt_description.Location = new System.Drawing.Point(21, 108);
            this.txt_description.Multiline = true;
            this.txt_description.Name = "txt_description";
            this.txt_description.Size = new System.Drawing.Size(416, 100);
            this.txt_description.TabIndex = 3;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(543, 204);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(92, 40);
            this.button1.TabIndex = 14;
            this.button1.Text = "Submit";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(394, 78);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(57, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "My Project";
            // 
            // dt_completion_date
            // 
            this.dt_completion_date.Location = new System.Drawing.Point(166, 215);
            this.dt_completion_date.Name = "dt_completion_date";
            this.dt_completion_date.Size = new System.Drawing.Size(271, 20);
            this.dt_completion_date.TabIndex = 15;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(18, 221);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(126, 13);
            this.label8.TabIndex = 16;
            this.label8.Text = "Project Complition Date : ";
            // 
            // clm_title
            // 
            this.clm_title.HeaderText = "Project Title";
            this.clm_title.Name = "clm_title";
            this.clm_title.ReadOnly = true;
            // 
            // clm_description
            // 
            this.clm_description.HeaderText = "Project Description";
            this.clm_description.Name = "clm_description";
            this.clm_description.ReadOnly = true;
            // 
            // clm_start_date
            // 
            this.clm_start_date.HeaderText = "Project Start Date";
            this.clm_start_date.Name = "clm_start_date";
            this.clm_start_date.ReadOnly = true;
            // 
            // clm_end_date
            // 
            this.clm_end_date.HeaderText = "Project End Date";
            this.clm_end_date.Name = "clm_end_date";
            this.clm_end_date.ReadOnly = true;
            // 
            // clm_client
            // 
            this.clm_client.HeaderText = "Client Name";
            this.clm_client.Name = "clm_client";
            this.clm_client.ReadOnly = true;
            // 
            // programmer_my_projects
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(912, 536);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.grd_project_data);
            this.Name = "programmer_my_projects";
            this.Text = "programmer_my_projects";
            this.Load += new System.EventHandler(this.programmer_my_projects_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grd_project_data)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView grd_project_data;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txt_description;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lbl_start_date;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lbl_client_name;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lbl_end_date;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbl_title;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DateTimePicker dt_completion_date;
        private System.Windows.Forms.DataGridViewTextBoxColumn clm_title;
        private System.Windows.Forms.DataGridViewTextBoxColumn clm_description;
        private System.Windows.Forms.DataGridViewTextBoxColumn clm_start_date;
        private System.Windows.Forms.DataGridViewTextBoxColumn clm_end_date;
        private System.Windows.Forms.DataGridViewTextBoxColumn clm_client;
    }
}