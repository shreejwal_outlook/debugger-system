﻿namespace bug_dubugger_system
{
    partial class project_manager_data
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(project_manager_data));
            this.grd_project_data = new System.Windows.Forms.DataGridView();
            this.clm_title = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clm_start_date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clm_end_date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clm_client = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clm_programmer_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clm_created_date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txt_title = new System.Windows.Forms.TextBox();
            this.txt_client_name = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.dt_start_date = new System.Windows.Forms.DateTimePicker();
            this.dt_end_date = new System.Windows.Forms.DateTimePicker();
            this.drp_programmer_id = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.homeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pMPanelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.grd_project_data)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grd_project_data
            // 
            this.grd_project_data.AllowUserToAddRows = false;
            this.grd_project_data.AllowUserToDeleteRows = false;
            this.grd_project_data.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grd_project_data.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clm_title,
            this.clm_start_date,
            this.clm_end_date,
            this.clm_client,
            this.clm_programmer_id,
            this.clm_created_date});
            this.grd_project_data.Location = new System.Drawing.Point(399, 187);
            this.grd_project_data.Name = "grd_project_data";
            this.grd_project_data.ReadOnly = true;
            this.grd_project_data.Size = new System.Drawing.Size(641, 217);
            this.grd_project_data.TabIndex = 0;
            this.grd_project_data.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grd_project_data_CellClick);
            this.grd_project_data.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grd_project_data_CellContentClick);
            // 
            // clm_title
            // 
            this.clm_title.HeaderText = "Project Title";
            this.clm_title.Name = "clm_title";
            this.clm_title.ReadOnly = true;
            // 
            // clm_start_date
            // 
            this.clm_start_date.HeaderText = "Project Start Date";
            this.clm_start_date.Name = "clm_start_date";
            this.clm_start_date.ReadOnly = true;
            // 
            // clm_end_date
            // 
            this.clm_end_date.HeaderText = "Project End Date";
            this.clm_end_date.Name = "clm_end_date";
            this.clm_end_date.ReadOnly = true;
            // 
            // clm_client
            // 
            this.clm_client.HeaderText = "Client Name";
            this.clm_client.Name = "clm_client";
            this.clm_client.ReadOnly = true;
            // 
            // clm_programmer_id
            // 
            this.clm_programmer_id.HeaderText = "Programmer ID";
            this.clm_programmer_id.Name = "clm_programmer_id";
            this.clm_programmer_id.ReadOnly = true;
            // 
            // clm_created_date
            // 
            this.clm_created_date.HeaderText = "Created Date";
            this.clm_created_date.Name = "clm_created_date";
            this.clm_created_date.ReadOnly = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.drp_programmer_id);
            this.groupBox1.Controls.Add(this.dt_end_date);
            this.groupBox1.Controls.Add(this.dt_start_date);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txt_client_name);
            this.groupBox1.Controls.Add(this.txt_title);
            this.groupBox1.Location = new System.Drawing.Point(38, 176);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(345, 266);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // txt_title
            // 
            this.txt_title.Location = new System.Drawing.Point(145, 48);
            this.txt_title.Name = "txt_title";
            this.txt_title.Size = new System.Drawing.Size(100, 20);
            this.txt_title.TabIndex = 2;
            this.txt_title.TextChanged += new System.EventHandler(this.txt_title_TextChanged);
            // 
            // txt_client_name
            // 
            this.txt_client_name.Location = new System.Drawing.Point(145, 153);
            this.txt_client_name.Name = "txt_client_name";
            this.txt_client_name.Size = new System.Drawing.Size(100, 20);
            this.txt_client_name.TabIndex = 5;
            this.txt_client_name.TextChanged += new System.EventHandler(this.textBox4_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.label1.Location = new System.Drawing.Point(413, 123);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(247, 31);
            this.label1.TabIndex = 2;
            this.label1.Text = "Project Data View";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(60, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Project Title";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(48, 88);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Project Start Date";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(48, 122);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Project End Date";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(72, 156);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Client Name";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(60, 182);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Programmer ID";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // dt_start_date
            // 
            this.dt_start_date.Location = new System.Drawing.Point(145, 82);
            this.dt_start_date.Name = "dt_start_date";
            this.dt_start_date.Size = new System.Drawing.Size(188, 20);
            this.dt_start_date.TabIndex = 3;
            this.dt_start_date.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // dt_end_date
            // 
            this.dt_end_date.Location = new System.Drawing.Point(145, 116);
            this.dt_end_date.Name = "dt_end_date";
            this.dt_end_date.Size = new System.Drawing.Size(188, 20);
            this.dt_end_date.TabIndex = 14;
            this.dt_end_date.ValueChanged += new System.EventHandler(this.dateTimePicker2_ValueChanged);
            // 
            // drp_programmer_id
            // 
            this.drp_programmer_id.FormattingEnabled = true;
            this.drp_programmer_id.Location = new System.Drawing.Point(145, 179);
            this.drp_programmer_id.Name = "drp_programmer_id";
            this.drp_programmer_id.Size = new System.Drawing.Size(121, 21);
            this.drp_programmer_id.TabIndex = 15;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.Location = new System.Drawing.Point(399, 419);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(83, 31);
            this.button1.TabIndex = 3;
            this.button1.Text = "Update";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.Maroon;
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.Location = new System.Drawing.Point(503, 419);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(70, 31);
            this.button2.TabIndex = 4;
            this.button2.Text = "Delete";
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.homeToolStripMenuItem,
            this.pMPanelToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1052, 24);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // homeToolStripMenuItem
            // 
            this.homeToolStripMenuItem.Name = "homeToolStripMenuItem";
            this.homeToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.homeToolStripMenuItem.Text = "Home";
            // 
            // pMPanelToolStripMenuItem
            // 
            this.pMPanelToolStripMenuItem.Name = "pMPanelToolStripMenuItem";
            this.pMPanelToolStripMenuItem.Size = new System.Drawing.Size(69, 20);
            this.pMPanelToolStripMenuItem.Text = "PM Panel";
            this.pMPanelToolStripMenuItem.Click += new System.EventHandler(this.pMPanelToolStripMenuItem_Click);
            // 
            // project_manager_data
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(1052, 461);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.grd_project_data);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "project_manager_data";
            this.Text = "project_manager_data";
            this.Load += new System.EventHandler(this.project_manager_data_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grd_project_data)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView grd_project_data;
        private System.Windows.Forms.DataGridViewTextBoxColumn clm_title;
        private System.Windows.Forms.DataGridViewTextBoxColumn clm_start_date;
        private System.Windows.Forms.DataGridViewTextBoxColumn clm_end_date;
        private System.Windows.Forms.DataGridViewTextBoxColumn clm_client;
        private System.Windows.Forms.DataGridViewTextBoxColumn clm_programmer_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn clm_created_date;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_client_name;
        private System.Windows.Forms.TextBox txt_title;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dt_end_date;
        private System.Windows.Forms.DateTimePicker dt_start_date;
        private System.Windows.Forms.ComboBox drp_programmer_id;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem homeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pMPanelToolStripMenuItem;
    }
}