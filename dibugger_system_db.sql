create database debugger_db ; 
use debugger_db;



select * from user_registration ; 
drop table users ; 

select * from users ; 
delete from  users ; 


create table users (
	id int identity(1,1)
	,first_name varchar(200) not null
	,last_name  varchar(200) not null
	,email varchar(200) not null
	,address varchar(200) not null
	,phone_no varchar(200) not null
	,user_type char(2) not null
	,[password] varchar(20) not null
	,created_date datetime default getdate()
	,modified_date datetime
);

select * from  project_registration ; 
drop table project_registration ; 
create table project_registration (
	id int identity(1,1)
	,title varchar(100) not null 
	,start_date datetime not null
	,end_date datetime not null
	,client_name varchar(30)
	,[description] varchar(255)
	,programmer_id varchar(200) not null 
	,created_date datetime default getdate()
	,modified_date datetime 
); 

create table programmer_projects(
id int identity(1,1)
,project_registration_id  int not null 
,completion_date datetime not null 
); 

select * from programmer_projects ; 
select * from project_registration ; 
 
SELECT title,start_date,end_date,client_name,programmer_projects.completion_date  from project_registration
FULL OUTER JOIN programmer_projects ON project_registration.id = programmer_projects.project_registration_id;


drop proc user_p ; 

create proc user_p 
(
@char char(15) = null
,@user_id int = null
,@first_name varchar(200) = null
,@last_name varchar(200) = null 
,@email varchar(200) = null
,@address varchar(200) = null
,@phone_no varchar(200) = null 
,@user_type char(2) = null 
,@password varchar(20) = null
,@modified_date datetime = null
)
as 
if (@char = 'insert') 
begin
INSERT INTO [dbo].[users]
           ([first_name]
           ,[last_name]
           ,[email]
           ,[address]
           ,[phone_no]
           ,[user_type]
		   ,[password]
           ,[modified_date])
     VALUES
           (
		   @first_name 
		  ,@last_name 
		  ,@email 
		  ,@address 
	      ,@phone_no 
		  ,@user_type 
		  ,@password
		  ,@modified_date 
)
end 
else if (@char = 'programmer')
begin
select * from users where user_type = 'P' ; 
end
else if (@char = 'view_s')
begin 
select * from users where email = @email ; 
end 
else if (@char = 'v_id')
begin
select * from users where id = @user_id ;  
end


create proc login_p
(
@email_id varchar(200)
,@password varchar(200)
)
as 
begin
select * from users where email = @email_id and [password] = @password
end

drop procedure project_manager_p  ;
select * from project_registration ; 

 drop procedure project_manager_p ; 
create proc project_manager_p 
(
	@char char(10) = null
	,@id int = null
	,@title varchar(100)  = null
	,@start_date datetime = null
	,@end_date datetime = null
	,@client_name varchar(30) = null
	,@description varchar(255) = null
	,@programmer_id varchar(200) = null
	,@modified_date datetime = null
)
as 
if(@char = 'insert')
begin 
INSERT INTO [dbo].[project_registration]
           ([title]
           ,[start_date]
           ,[end_date]
           ,[client_name]
           ,[description]
           ,[programmer_id]
           ,[modified_date])
     VALUES
           (
		    @title 
	,@start_date 
	,@end_date 
	,@client_name 
	,@description 
	,@programmer_id 
	,@modified_date
	)
end 
else if (@char = 'update')
begin 
UPDATE [dbo].[project_registration]
   SET [title] =  @title 
      ,[start_date] = @start_date 
      ,[end_date] = @end_date
      ,[client_name] = @client_name
      ,[description] = @description
      ,[programmer_id] = @programmer_id
      ,[modified_date] = @modified_date
 WHERE id = @id
end
else if (@char = 'delete')
begin 
delete from project_registration where id = @id ; 
end
else if (@char = 'view')
begin
select * from project_registration ; 
end 
else if (@char = 'my_p_v')
begin 
select * from project_registration where programmer_id = @programmer_id; 
end 


drop proc programmer_project_P ; 
create proc programmer_project_p
(
@char char(15) = null
,@project_registration_id int = null 
,@completion_date datetime = null 
)
as
if(@char = 'insert')
begin 
INSERT INTO [dbo].[programmer_projects]
           ([project_registration_id]
           ,[completion_date])
     VALUES
           (
		   @project_registration_id 
           ,@completion_date )
end
else if (@char = 'pm_p_view') 
begin 
SELECT title,start_date,end_date,client_name,programmer_id,programmer_projects.completion_date  from project_registration
FULL OUTER JOIN programmer_projects ON project_registration.id = programmer_projects.project_registration_id;
end 
select * from project_registration;